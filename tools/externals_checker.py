import argparse
import os
import gitlab
import pandas as pd
import dataframe_image as dfi
from tabulate import tabulate


def externals_checker(oauth_token=None, CMakeCache_dir=None, output_format="image", output_dir="."):
    file = open(CMakeCache_dir + "/CMakeCache.txt", "r")
    versions_file = open("./../versions.md", "w")
    Lines = file.readlines()

    if oauth_token:
        gl = gitlab.Gitlab("https://gitlab.cern.ch", oauth_token=oauth_token)
    elif os.environ["CI_JOB_TOKEN"]:
        gl = gitlab.Gitlab(
            "https://gitlab.cern.ch", job_token=os.environ["CI_JOB_TOKEN"]
        )
    else:
        raise ValueError(
            "Either pass a CI_JOB_TOKEN as environment variable by running in the gitlab CI, or pass a personal token with --oauth_token"
        )

    count = 0
    df = pd.DataFrame(
        columns=[
            "software",
            "included ref",
            "included ref date",
            "latest ref",
            "latest ref date",
        ]
    )
    # Strips the newline character
    for line in Lines:
        if "STATANA" not in line:
            continue

        if "REPOSITORY" in line and "gitlab.cern.ch:7999" in line:
            proj_and_namespace = (
                line.split("gitlab.cern.ch:7999/", 1)[1]
                .replace(".git", "")
                .replace("\n", "")
            )
            try:
                proj = gl.projects.get(proj_and_namespace)
            except gitlab.exceptions.GitlabGetError as e:
                print("The following exception in retrieving the repository was encountered, continuing for now: ", e)
                continue

            # First we get included ref in repo
            commit_string = [
                ver_line
                for ver_line in Lines
                if line.split("_REPOSITORY", 1)[0] + "_VERSION" in ver_line
            ][0]
            commit_string = commit_string[commit_string.find("=") + 1 :].replace(
                "\n", ""
            )

            commit = proj.commits.get(commit_string)
            current_tag = commit.refs("tag")  # only tags
            current_branch = commit.refs("branch")  # only tags
            if current_tag:
                current_tag_name = current_tag[0]["name"]
                current_tag_date = commit.created_at
                # get latest tag
                latest_tag = proj.tags.list(get_all=False)[0]
                latest_tag_name = latest_tag.name
                latest_tag_date = latest_tag.commit["created_at"]

                df = df.append(
                    {
                        "software": proj.name,
                        "included ref": "   " + current_tag_name,
                        "included ref date": current_tag_date.split("T", 1)[0],
                        "latest ref": "   " + latest_tag_name,
                        "latest ref date": latest_tag_date.split("T", 1)[0],
                    },
                    ignore_index=True,
                )

            elif current_branch:
                current_commit_name = commit.short_id
                current_commit_date = commit.created_at

                # get latest commit
                latest_commit = proj.commits.list(sort="committed_date", get_all=False)[
                    0
                ]
                latest_commit_name = latest_commit.short_id
                latest_commit_date = latest_commit.created_at

                df = df.append(
                    {
                        "software": proj.name,
                        "included ref": "   " + commit.refs("branch")[0][
                            "name"
                        ]
                        + " "
                        + current_commit_name,
                        "included ref date": current_commit_date.split("T", 1)[0],
                        "latest ref": "   " + latest_commit.refs("branch")[
                            0
                        ]["name"]
                        + " "
                        + latest_commit_name,
                        "latest ref date": latest_commit_date.split("T", 1)[0],
                    },
                    ignore_index=True,
                )

    for line in Lines:
        if "_VERSION:STRING" in line and "STATANA" in line:
            version_name = line[line.find("=") + 1 :].replace("\n", "")
            software_name = line[line.find("_") + 1 :]
            software_name = software_name[: software_name.find("_")]

            if software_name in list(df.software.str.upper()):
                continue

            df = df.append(
                {
                    "software": software_name,
                    "included ref": "   " + version_name,
                    "included ref date": "",
                    "latest ref": "   " + "",
                    "latest ref date": "",
                },
                ignore_index=True,
            )

    pd.set_option("display.max_colwidth", None)
    pd.set_option('display.width', -1)
    pd.set_option('display.max_rows', None)
    versions_file.write(df.to_markdown())

    if output_format == "image" or  output_format == "both":
        dfi.export(df, output_dir + "/included-versions.png", table_conversion = 'matplotlib', fontsize=8)
    if output_format == "text" or output_format == "both":
        with open(output_dir + "/included-versions.txt", 'w') as f:
            f.write(tabulate(df, headers='keys', showindex=False))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="externals checker.")
    parser.add_argument(
        "--oauth_token",
        type=str,
        nargs="?",
        help="oauth token for logging into gitlab",
        default=None,
    )
    parser.add_argument(
        "--CMakeCache_dir",
        type=str,
        nargs="?",
        help="directory containing CMakeCache.txt file",
        default=None,
    )
    parser.add_argument(
        "--output_format",
        type=str,
        nargs="?",
        help="format to output versions in, can be one of `image` or `text`",
        default="image",
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        nargs="?",
        help="directory to output version file(s) to",
        default=".",
    )

    args = vars(parser.parse_args())

    externals_checker(**args)
