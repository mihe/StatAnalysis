#!/bin/bash

cd `dirname "$0"`

# Loop over all tests
for f in $(find . -type f -name "*.cpp") ; do

    # ensure we do not recursively run this script
    if [[ "$f" == './test.sh' ]] || [[ "$f" == './compare.py' ]]; then
        continue
    fi

    # delete any existing results file before running test
    rm -f results.csv

    printf "Within ROOT, running the test: $f \n"

    # run the tests and suppress output
    root -q $f > /dev/null

    # check difference between generated output and precalculated correct output
    ./compare.py results_correct.csv results.csv

    # exit with status code 1 on failure of compare.py
    if [[ $? == 1 ]]; then
        exit 1
    fi

    printf "====================================="

done
