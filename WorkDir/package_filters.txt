# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Filter for building just the RooFitExtensions package on top of an existing
# AtlasStats release.
#

# The selected package(s).
+ RooFitExtensions

# Everything else is vetoed.
- .*
